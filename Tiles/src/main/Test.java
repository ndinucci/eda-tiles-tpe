package main;

import game.Board;

import java.util.ArrayList;
import java.util.List;

import minimax.Minimax;

public class Test {
	public static void main(String[] args) {
		int i, j, k, l;
		int dimMax=8;
		int coloresMax=4;
		int cantPruebas=1000;
		int depth=5;
		int[][][] tiempos = new int[coloresMax-1][depth*2][dimMax-3];
		List<Minimax> mMs = new ArrayList<Minimax>(depth*2);
		for(i=1;i<=depth;i++){
			mMs.add(new Minimax(true, i, false, false));
			mMs.add(new Minimax(true, i, true, false));
		}
		for (i = 2; i <= coloresMax; i++) {
			for (j = 4; j <= dimMax; j++) {
				for (l = 0; l < depth*2; l++) {
					tiempos[i-2][l][j-4]=0;
				}
			}
		}
		for (i = 2; i <= coloresMax; i++) {
			for (j = 4; j <= dimMax; j++) {
				for (k = 0; k < cantPruebas; k++) {
					Board board = new Board(j, j, i);
					for (l = 0; l < depth*2; l++) {
						long end;
						long start = System.currentTimeMillis();
						mMs.get(l).bestPlayFound(board);
						end = System.currentTimeMillis();
						tiempos[i-2][l][j-4] += (end - start);
					}
				}
			}
		}
		for (i = 2; i <= coloresMax; i++) {
			System.out.println();
			System.out.println("Con "+i+" colores:");
			for (j = 4; j <= dimMax; j++) {
				System.out.println();
				for (l = 0; l < depth*2; l++) {
					System.out.print(((double)tiempos[i-2][l][j-4])/cantPruebas+" ");
				}
			}
		}
	}
}
