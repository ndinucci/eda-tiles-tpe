package visual;

import game.Board;

import java.awt.BorderLayout;

import javax.swing.JFrame;

import minimax.Minimax;




public class GameFrame extends JFrame {

	private static final long serialVersionUID = 1L;

	public GameFrame(Board board, Minimax mM) throws InterruptedException {
		this.setTitle("Tiles: Grupo 1");
		setLayout(new BorderLayout());
		setResizable(false);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		GeneralPanel gp = new GeneralPanel(board, mM);
		add(gp, BorderLayout.CENTER);
		pack();
		setVisible(true);
	}

}
