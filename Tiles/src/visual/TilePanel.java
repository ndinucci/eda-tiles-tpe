package visual;

import game.Board;
import game.Play;

import java.awt.BorderLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JPanel;


public class TilePanel extends JPanel {

	private static final long serialVersionUID = 1L;

	public TilePanel(final Board board, final Play play, final GeneralPanel gp) {
		this.setLayout(new BorderLayout());
		this.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {
			}

			@Override
			public void mousePressed(MouseEvent e) {
			}

			@Override
			public void mouseExited(MouseEvent e) {

			}

			@Override
			public void mouseEntered(MouseEvent e) {
			}

			@Override
			public void mouseClicked(MouseEvent e) {
				if (!board.isP1Turn()) {
					if (board.makePlay(play)) {
						gp.setVisible(false); 			// no usamos el repaint porque
						gp.update(gp.getGraphics()); 	// como ve 2 muy proximos los
						gp.setVisible(true); 			// une solo haciendo el ultimo
						try {
							Thread.sleep(1000); 		// espera 1 segundo para que el
														// jugador llegue a ver el tablero
						} catch (InterruptedException a) {
							a.printStackTrace();
						}
						if (board.makePlay(gp.getmM().bestPlayFound(board))) {
							gp.setVisible(false); 			// a setVisible le cambiamos
							gp.update(gp.getGraphics()); 	// el valor para que no
							gp.setVisible(true);			// titile
						}
					}
				}
			}
		});
	}

}
