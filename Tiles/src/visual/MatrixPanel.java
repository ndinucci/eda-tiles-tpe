package visual;


import game.Board;
import game.Play;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;


public class MatrixPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	private Board board;

	public MatrixPanel(Board board, GeneralPanel gp, int width, int height) {
		this.board = board;
		this.setLayout(null);
		int i = 0, j = 0;
		int tileWidth = width/board.getDimCols();
		int tileHeight = height/board.getDimRows();
		while(j < board.getDimCols()){
			while( i < board.getDimRows()){
				TilePanel tp = new TilePanel(board, new Play(i, j), gp);
				tp.setBounds(j*tileWidth, 2+i*tileHeight, tileWidth-3, tileHeight-3);
				this.add(tp);
				i++;
			}
			i=0;
			j++;
		}
	}
	
	@Override
	public void paint(Graphics g) {
		int tileWidth = this.getWidth()/board.getDimCols();
		int tileHeight = this.getHeight()/board.getDimRows();
		int i=0, j=0;
		char c;
		while(j < board.getDimCols()){
			while( i < board.getDimRows()){
				c = board.getBoard()[j][i];
				g.setColor(getColor(c));
				g.fillRect(j*tileWidth, 2+i*tileHeight, tileWidth-3, tileHeight-3);
				i++;
			}
			i=0;
			j++;
		}
	}
	
	public Color getColor(char c){
		switch(c){
		case 0: return Color.white;
		case '1': return Color.red;
		case '2': return Color.blue;
		case '3': return Color.green;
		case '4': return Color.yellow;
		case '5': return Color.magenta;
		case '6': return Color.pink;
		case '7': return Color.darkGray;
		case '8': return Color.lightGray;
		default: return Color.cyan;
		
		}
	}

}
